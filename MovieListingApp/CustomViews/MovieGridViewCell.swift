//
//  MovieGridViewCell.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import UIKit

class MovieGridViewCell: UICollectionViewCell {
    
    @IBOutlet weak var favImageView: UIImageView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func populateViews(movie: Movie) {
        if let posterPath = movie.posterImageUrl() {
            let url = URL(string:posterPath)!
            posterImageView.af_setImage(
                withURL: url,
                placeholderImage: UIImage(named: "movie100brown")!,
                imageTransition: .crossDissolve(0.2)
            )
        }
        titleLabel?.text = movie.title
        
        DBManager.sharedInstance.getData(sectionName: movie.id!) { [weak self] (favs, isAvailable) in
            if isAvailable {
                if(favs.isFaved){
                    self?.favImageView.isHidden = false
                }else{
                    self?.favImageView.isHidden = true
                }
            }else{
                self?.favImageView.isHidden = true
            }
        }
   
    }
}
