//
//  ApiManager.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import Foundation
import Alamofire
import SwiftyJSON

class ApiManager {
    
    typealias MoviesListAPIResult = (moviesList:[Movie] , nextPage: Int?, totalPages:Int)
    
    class func getMoviesList (pageNumber: Int ,completion: @escaping (MoviesListAPIResult?) -> Void) {
        let moviesURL = getMoviesListURL(page: pageNumber)
        AF.request(moviesURL, method: .get,  parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    var moviesList: [Movie] = []
                    switch response.result {
                    case .success(let value):
                        if let json = value as? [String: Any] {
                            let json = JSON(json)
                            if let moviesJsonList = json["results"].array {
                                for movie in moviesJsonList {
                                    moviesList.append(Movie(movieJson: movie))
                                }
                            }
                            let currentPage = json["page"].int!
                            let totalPages = json["total_pages"].int!
                            let nextPage = currentPage+1 < totalPages ? currentPage+1 : nil
                            completion((moviesList , nextPage , totalPages))
                        }else{
                            completion(nil)
                        }
                    case .failure(let error):
                        print(error)
                        completion(nil)
                    }
            }
        /*
        AF.request(moviesURL).responseJSON { response in
            var moviesList: [Movie] = []
            if response.result.isSuccess {
                let json = JSON(response.result.value!)
                if let moviesJsonList = json["results"].array {
                    for movie in moviesJsonList {
                        moviesList.append(Movie(movieJson: movie))
                    }
                }
                let currentPage = json["page"].int!
                let totalPages = json["total_pages"].int!
                let nextPage = currentPage+1 < totalPages ? currentPage+1 : nil
                completion((moviesList , nextPage , totalPages))
            } else {
                completion(nil)
            }
        }
        */
    }
    
    class func getMoreDetailedMovie (movieId : Int, completion: @escaping (Movie?) -> Void) {
        let moviesURL = getMovieDetailsUrl(movieId: movieId)
        
        AF.request(moviesURL, method: .get,  parameters: nil, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        if let json = value as? [String: Any] {
                            completion (Movie(movieJson: JSON(json)))
                        }else{
                            completion(nil)
                        }
                    case .failure(let error):
                        completion(nil)
                    }
            }
        
        
        /*
        AF.request(moviesURL).responseJSON { response in
            if response.result.isSuccess{
                completion (Movie(movieJson: JSON(response.result.value!)))
            }
            else{
                completion(nil)
            }
        }
        */
    }

    class func getMovieDetailsUrl(movieId: Int) -> String {
        return "\(appendAPIKeyToURL(url: "\(Constants.BASE_URL)\(movieId)?language=en-US", isOneQueryParam: false))&append_to_response=videos"
    }
    

    class func getPosterImageUrl(imagePath: String) -> String {
        return "\(Constants.POSTER_BASE_URL)\(imagePath)"
    }
    
    class func getBackDropImageUrl(imagePath: String) -> String {
        return "\(Constants.POSTER_DETAIL_BASE_URL)\(imagePath)"
    }
    
    class func getMoviesListURL(page:Int) -> String {
        return appendAPIKeyToURL(url:"\(Constants.BASE_URL)popular?language=en-US&page=\(page)" , isOneQueryParam: false)
    }
    
    class func appendAPIKeyToURL(url: String, isOneQueryParam: Bool) -> String {
        let separator = isOneQueryParam ? "?" : "&"
        return "\(url)\(separator)api_key=\(Constants.API_KEY)"
    }
}

