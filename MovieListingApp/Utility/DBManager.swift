//
//  DBManager.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    let dateFormatter = DateFormatter()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataFromDB() -> Results<Favs> {
        
        let results: Results<Favs> = database.objects(Favs.self)
        return results
    }
    
    func getData(sectionName:Int,completion: @escaping (_ Data: Favs, _ isContain:Bool) -> Void){
            let realm = try! Realm()
            let data = realm.object(ofType: Favs.self, forPrimaryKey: sectionName)
            if data != nil{
                completion(data!, true)
            }else{
                completion(Favs(), false)
            }

        }
    
    func addData(object: Favs) {
        
        try! database.write {
            database.add(object)
            print("Added new object")
        }
    }
    
    func updateData(object: Favs, isFaved: Bool) {
        
        try! database.write {
            object.isFaved = isFaved
            database.add(object, update: .modified)
            print("Added new object")
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: Favs) {
        
        try! database.write {
            
            database.delete(object)
        }
    }
    
}
