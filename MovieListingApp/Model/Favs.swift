//
//  Favs.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import RealmSwift

class Favs: Object {
        @objc dynamic var ID = 0
        @objc dynamic var isFaved = false
    
        override static func primaryKey() -> String? {
              return "ID"
        }

}
