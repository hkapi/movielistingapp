//
//  Movie.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import Foundation
import SwiftyJSON

struct Movie {
    var title: String?
    var overview: String?
    var posterPath: String?
    var backdropPath: String?
    var vote_count: Double?
    var id: Int?
    
    
    init(movieJson: JSON) {
        if let movieJsonDict = movieJson.dictionaryObject {
            title =  movieJsonDict["title"] as? String
            id =  movieJsonDict["id"] as? Int
            overview =  movieJsonDict["overview"] as? String
            posterPath =  movieJsonDict["poster_path"] as? String
            backdropPath =  movieJsonDict["backdrop_path"] as? String
            vote_count =  movieJsonDict["vote_count"] as? Double
        }
    
    }
    
    func posterImageUrl() -> String? {
        if let posterPath = posterPath {
            return "\(Constants.POSTER_BASE_URL)\(posterPath)"
        }
        return nil
    }
    
    func backDropImageUrl() -> String? {
        if let backdropPath = backdropPath {
            return "\(Constants.POSTER_DETAIL_BASE_URL)\(backdropPath)"
        }
        return nil
    }
}
