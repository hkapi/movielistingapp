//
//  Constants.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import Foundation

struct Constants {
    static let API_KEY = "fd2b04342048fa2d5f728561866ad52a"
    static let BASE_URL = "https://api.themoviedb.org/3/movie/"
    static let MOVIE_SEARCH_BASE_URL = "https://api.themoviedb.org/3/search/movie"
    static let IMAGES_BASE_URL = "https://image.tmdb.org/t/p/"
    static let POSTER_DETAIL_BASE_URL = IMAGES_BASE_URL + "w500"
    static let POSTER_BASE_URL = IMAGES_BASE_URL + "w500"
}
