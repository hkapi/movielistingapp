//
//  MovieDetailsViewController.swift
//  MovieListingApp
//
//  Created by Huseyin Kapi on 4.12.2020.
//

import UIKit

class MovieDetailsViewController: UIViewController {
  
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var movieSummaryLabel: UILabel!
    
    var movie: Movie!
    
    
    @IBOutlet weak var favBtn: UIButton!
    @IBAction func favBtnDidTap(_ sender: Any) {
        
        DBManager.sharedInstance.getData(sectionName: movie.id!) { [weak self] (favs, isAvailable) in
            let object: Favs = Favs()
            object.ID = (self?.movie.id!)!
            
            if isAvailable {
                if(favs.isFaved){
                    self?.favBtn.setImage(UIImage.init(named: "unfavorite"), for: .normal)
                    object.isFaved = false
                    DBManager.sharedInstance.updateData(object: object, isFaved: false)
                }else{
                    object.isFaved = true
                    self?.favBtn.setImage(UIImage.init(named: "favorite"), for: .normal)
                    DBManager.sharedInstance.updateData(object: object, isFaved: true)
                }
            }else{
                object.isFaved = true
                self?.favBtn.setImage(UIImage.init(named: "favorite"), for: .normal)
                DBManager.sharedInstance.addData(object: object)
            }
            
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateViews()
        loadMoreDetailedMovie()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateViews(){
        title = movie.title
    
        if let posterPath = movie.posterImageUrl() {
            let url = URL(string:posterPath)!
            posterImageView.af_setImage(withURL: url,imageTransition: .crossDissolve(0.2) )
        }
        titleLabel.text = movie.title
        ratingsLabel.text = String(format: " %.f votes", movie.vote_count!)
        movieSummaryLabel.text = movie.overview
        
        DBManager.sharedInstance.getData(sectionName: movie.id!) { [weak self] (favs, isAvailable) in
            if isAvailable {
                if(favs.isFaved){
                    self?.favBtn.setImage(UIImage.init(named: "favorite"), for: .normal)
                }else{
                    self?.favBtn.setImage(UIImage.init(named: "unfavorite"), for: .normal)
                }
            }else{
                self?.favBtn.setImage(UIImage.init(named: "unfavorite"), for: .normal)
            }
        }
    }
    

  
    
    func loadMoreDetailedMovie(){
        ApiManager.getMoreDetailedMovie(movieId: movie.id!) { (movie) in
            if let movie = movie {
                self.movie = movie
                self.populateViews()
            }else{
       
                //TODO
            }
        }
    }
}

