# MovieListingApp

**MovieListingApp** is the app showing the list the popular movies and their information using the [TMDB API]

## Features
- TableView and CollectionView are used for the grid and vertical listing of the movies. Their delegate and datasources are also defined in the MovieListViewController extension. 
- UIRefreshControl is used for controlling the scrolls and fetching the other pages on  the tableView(list view) and collectionView (grid view). This component is used for the "Load More" effect.
- The favorites data is stored on the device locally by using DBManager.
- ApiManager handles the api requests and manages the required url fetcing.
- Movie model object is used for storing each moview object fetched from the api results.
- Favs is a realm object to store the favorite information in device locally.


## Technical Info
**MVC** pattern is used.

**Alamofire** is used for http requests.

**SwiftyJSON** is used for JSON parsing

**Realm** is used for favourite list.


